-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- โฮสต์: localhost
-- เวลาในการสร้าง: 19 ม.ค. 2013  น.
-- รุ่นของเซิร์ฟเวอร์: 5.0.51
-- รุ่นของ PHP: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- ฐานข้อมูล: `userdb`
-- 

-- --------------------------------------------------------

-- 
-- โครงสร้างตาราง `usertb`
-- 

CREATE TABLE `usertb` (
  `id` int(11) NOT NULL auto_increment,
  `firstname` varchar(30) collate utf8_unicode_ci NOT NULL,
  `lastname` varchar(50) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

-- 
-- dump ตาราง `usertb`
-- 

INSERT INTO `usertb` VALUES (1, '111', '222');
INSERT INTO `usertb` VALUES (4, 'aaa', 'ccc');
INSERT INTO `usertb` VALUES (6, 'aRomSia', 'Teesood');
